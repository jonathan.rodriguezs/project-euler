#include <stdio.h>

// @Title  Bruteforce algorithm
// @author jonrodsanz
// @desc   Find all the sums from 1 below limit which
//         are multiples of m0 or m1
int sum_multiples(int m0, int m1, int limit){
  int sum = 0;
  for(int i = 1; i < limit; i++){
    if((i % m0) == 0 || (i % m1) == 0){
      sum += i;
    }
  }
  return sum;
}

// @Title  Arithmetic approach algorithm
// @author https://www.mathblog.dk/project-euler-problem-1/
// @desc   Sum of the multiples of n beginnig at 1 to p
int summatory(int n, int p){
  return n*(p/n)*(p/n+1)/2;
}

int sum_multiples_arithmetic(int m0, int m1, int limit){
  return summatory(m0, limit) + summatory(m1, limit) - summatory(m0 * m1, limit);
}

int main(void) {
  int m0 = 3;
  int m1 = 5;
  int limit = 1000;
  int result = sum_multiples_arithmetic(m0, m1, limit - 1);
  printf("The sum of all the multiples of %i or %i below %i is: %i\n", m0, m1, limit, result);
  return 0;
}
